﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletController : MonoBehaviour
{
    public float speed = 5;
    // Update is called once per frame
    void Update()
    {
        MoveBullet();
    }

    void MoveBullet()
    {
        Vector3 tempPosition = transform.position;
        tempPosition += transform.rotation * (Vector3.forward * speed) * Time.deltaTime;
        transform.position = tempPosition;
    }

    void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Enemy")
        {
            Destroy(other.gameObject);
            Destroy(gameObject);
        }
        if (other.gameObject.tag == "Barykada")
        {
            
            Destroy(gameObject);
        }
    }
}
