﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawnerController : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField]
    private float spawnTime = 1;
    public GameObject prefabEnemy;
    public GameObject[] spawnPositionObjeccts;

    void Start()
    {
        Invoke("EnemySpawner", spawnTime);
    }


    // Update is called once per frame
    void Update()
    {
       // EnemySpawner();
    }

    void EnemySpawner()
    {
        int numberRandom = Random.Range(0, spawnPositionObjeccts.Length);
        Debug.Log(numberRandom);
        GameObject spawnPosition = spawnPositionObjeccts[numberRandom].gameObject;
        Instantiate(prefabEnemy, spawnPosition.transform.position,spawnPosition.transform.rotation);

        Invoke("EnemySpawner", spawnTime);
    }

}
