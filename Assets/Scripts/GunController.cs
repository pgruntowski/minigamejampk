﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunController : MonoBehaviour
{
    [SerializeField]
    private float speed = 10;
    public FixedJoystick fixedJoystick;
    public GameObject bulletSpawnPoint;
    public GameObject bulletPrefab;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        GunRotateUpdate();
    }

    void GunRotateUpdate()
    {
        Vector3 tempEulerAngles = transform.localRotation.eulerAngles;
        tempEulerAngles.y += speed * Time.deltaTime * fixedJoystick.Horizontal;
        tempEulerAngles.y = (tempEulerAngles.y > 180) ? tempEulerAngles.y - 360 : tempEulerAngles.y;
        tempEulerAngles.y = Mathf.Clamp(tempEulerAngles.y, -70f, 70f);
        transform.localEulerAngles = tempEulerAngles;
    }

    public void GunFire()
    {
        Instantiate(bulletPrefab, bulletSpawnPoint.transform.position, bulletSpawnPoint.transform.rotation);
    }




}
